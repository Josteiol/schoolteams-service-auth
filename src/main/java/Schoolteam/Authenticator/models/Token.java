package Schoolteam.Authenticator.models;

public class Token {

    //Token string, service string,

    public String tokenstring;
    public String service;

    Token() {}

    Token(String tokenstring, String service){
        this.tokenstring = tokenstring;
        this.service = service;
    }

    public String getTokenstring(){
        return this.tokenstring;
    }

    public void setTokenstring(String tokenstring){
        this.tokenstring = tokenstring;
    }

    public String getService(){
        return this.service;
    }

    public void setService(String service) {
        this.service = service;
    }
}
