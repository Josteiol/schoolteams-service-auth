package Schoolteam.Authenticator.models;

public class TokenReturn {
    String token;

    public TokenReturn(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
