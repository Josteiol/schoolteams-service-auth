package Schoolteam.Authenticator.models;

public class RegisterUser {

    private String username;
    private String password;
    private String emailAddress;
    private int role_id;
    private String parent1;
    private String parent2;
    private String firstname;
    private String lastname;
    private boolean notify;

    public RegisterUser(String username, String password, String emailAddress, int role_id, String parent1, String parent2, String firstname, String lastname, boolean notify) {
        this.username = username;
        this.password = password;
        this.emailAddress = emailAddress;
        this.role_id = role_id;
        this.parent1 = parent1;
        this.parent2 = parent2;
        this.firstname = firstname;
        this.lastname = lastname;
        this.notify = notify;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public int getRole_id() {
        return role_id;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }

    public String getParent1() {
        return parent1;
    }

    public void setParent1(String parent1) {
        this.parent1 = parent1;
    }

    public String getParent2() { return parent2; }

    public void setParent2(String parent2) { this.parent2 = parent2; }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public boolean isNotify() {
        return notify;
    }

    public void setNotify(boolean notify) {
        this.notify = notify;
    }
}
