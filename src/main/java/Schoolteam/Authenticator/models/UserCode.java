package Schoolteam.Authenticator.models;

public class UserCode {

    String token;
    int code;

    public UserCode(String token, int code) {
        this.token = token;
        this.code = code;
    }

    public UserCode() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
