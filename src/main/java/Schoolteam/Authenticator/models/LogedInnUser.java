package Schoolteam.Authenticator.models;

public class LogedInnUser {

    private String username;
    private String token;
    private int role_id;
    private Boolean auth;

    public LogedInnUser(String username, String token, int role_id, Boolean auth){
        this.username = username;
        this.token = token;
        this.role_id = role_id;
        this.auth = auth;
    }

    public LogedInnUser() {
    }

    public String getUsername(){
        return this.username;
    }

    public void setUsername(String username){
        this.username = username;
    }

    public String getToken(){
        return this.token;
    }

    public void setToken(String token){
        this.token = token;
    }

    public int getRole_id(){
        return this.role_id;
    }

    public void setRole_id(int role_id){
        this.role_id = role_id;
    }

    public Boolean getAuth() {
        return auth;
    }

    public void setAuth(Boolean auth) {
        this.auth = auth;
    }
}


