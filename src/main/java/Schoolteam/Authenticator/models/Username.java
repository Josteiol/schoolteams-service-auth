package Schoolteam.Authenticator.models;

public class Username {
    String username;

    public Username(String username) {
        this.username = username;
    }

    public Username() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
