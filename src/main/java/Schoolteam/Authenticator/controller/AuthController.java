package Schoolteam.Authenticator.controller;

import Schoolteam.Authenticator.logic.Authorization;
import Schoolteam.Authenticator.logic.JwtKey;
import Schoolteam.Authenticator.models.*;
import Schoolteam.Authenticator.repository.*;
import Schoolteam.Authenticator.twoFactorAuth.TfaGoogleAuth;
import com.google.common.hash.Hashing;
import io.jsonwebtoken.Claims;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import Schoolteam.Authenticator.repository.UserRepository;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

@RestController
@RequestMapping("/api/v1")
@CrossOrigin("*")
public class AuthController {

    private UserRepository userRepository = new UserRepository();
    private OptionalRepository optionalRepository = new OptionalRepository();
    private PersonRepository personRepository = new PersonRepository();




    @PostMapping("/register")
    public ResponseEntity<RegisterUser> register(@RequestBody RegisterUser registerUser) throws IOException {
        // TODO: Get the authorization header


        // Create optional object
        Optional optional = new Optional(null, null, null, null, null,null,null, null);

        // Register optional
        Pair<HttpStatus, Integer> res = optionalRepository.addOptional(optional);
        if(res.getU() != -1){ optional.setId(res.getU()); }
        if(res.getT() != HttpStatus.CREATED){ return new ResponseEntity<>(null, res.getT()); }

        // Create person object
        Person person = new Person(
                registerUser.getFirstname(),
                registerUser.getLastname(),
                registerUser.getEmailAddress(),
                null,
                optional.getId()
        );

        // Register person with optionalID
        Pair<HttpStatus, Integer> personRes = personRepository.addPerson(person);
        if(personRes.getU() != -1){ person.setId(personRes.getU()); }
        if(res.getT() != HttpStatus.CREATED){ return new ResponseEntity<>(null, res.getT()); }

        // Create user object
        String hashedPassword = Hashing.sha256().hashString(registerUser.getPassword(), StandardCharsets.UTF_8).toString();

        User user = new User(
                registerUser.getUsername(),
                hashedPassword,
                person.getId(),
                registerUser.getRole_id()
        );

        // Register user with personID
        HttpStatus status = userRepository.addUser(user);
        if(status != HttpStatus.CREATED){ return new ResponseEntity<>(null, HttpStatus.OK); }

        TfaGoogleAuth googleAuth = new TfaGoogleAuth();
        googleAuth.generateGoogleKey(registerUser.getUsername());

        if(registerUser.getRole_id() == 4 && registerUser.getParent1() !=null || registerUser.getParent2() != null){

            // Create child parent object
            ChildParent childParent = new ChildParent(
                    registerUser.getUsername(),
                    registerUser.getParent1(),
                    registerUser.getParent2()
            );

            // Register parent-child relationship
            HttpStatus status1 = userRepository.addChildParent(childParent);
            if(status1 != HttpStatus.CREATED){ return new ResponseEntity<>(null, status); }
        }


        // Send request to email service
        if(registerUser.isNotify()){
            URL url = new URL ("https://schoolteams-service-mail.herokuapp.com/api/v1/invite");
            HttpURLConnection con = (HttpURLConnection)url.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json; utf-8");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);
            String jsonInputString = "{\"username\": \"" +registerUser.getUsername()+ "\"}";

            try(OutputStream os = con.getOutputStream()) {
                byte[] input = jsonInputString.getBytes("utf-8");
                os.write(input, 0, input.length);
            }

            try(BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                System.out.println(response.toString());
            }

        }


        return new ResponseEntity<>(HttpStatus.OK);
    }



    @PostMapping("/login")
    public ResponseEntity<LogedInnUser> login(@RequestBody LoginUser inputuser){

        String hashedPassword = Hashing.sha256().hashString(inputuser.getPassword(), StandardCharsets.UTF_8).toString();
        User user = userRepository.getSpecificUser(inputuser.getUsername());

        if(user == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        if(inputuser.getUsername().equals(user.getUsername()) && hashedPassword.equals(user.getPassword())){
            String returnToken;
            LogedInnUser returnUser;
            if(!user.isAuth()){ //If the user have not added 2fa
                // Create token
                returnToken = "Bearer " + JwtKey.createJWT(user.getUsername(), System.getenv("JWT_ISSUER"), String.valueOf(user.getRole_id()), -1);
                // Build return object
                returnUser = new LogedInnUser(user.getUsername(), returnToken, user.getRole_id(), true);
            }else{
                // Create token
                returnToken = "Bearer " + JwtKey.createJWT(user.getUsername(), System.getenv("JWT_2FA_ISSUER"), String.valueOf(user.getRole_id()), -1);
                // Build return object
                returnUser = new LogedInnUser(user.getUsername(), returnToken, user.getRole_id(), false);
            }

            // Send response
            return new ResponseEntity<>(returnUser,HttpStatus.OK);
        }
        return new ResponseEntity<>(null,HttpStatus.UNAUTHORIZED);
    }


    @PostMapping("/verify")
    public ResponseEntity<Boolean> login(@RequestBody Token token){
        // Extract token and service
        String tokenstring = token.tokenstring;
        String service  = token.service;
        Claims claim;

        //System.out.println(JwtKey.createJWT("Jostein", System.getenv("JWT_ISSUER"),"1",-1)); // for testing

        // Check if token is valid
        if (tokenstring == null || tokenstring.isEmpty() || !tokenstring.startsWith("Bearer ")) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } else {
            tokenstring = tokenstring.replace("Bearer ", "");

            try{
                claim = JwtKey.decodeJWT(tokenstring);
                if(!claim.getIssuer().equals(System.getenv("JWT_ISSUER"))){
                    return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
                }
            }catch (Exception e){
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }
        }
        // Check if user has access to service
        if(!Authorization.authorize(service, claim.getSubject())){
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }



}
