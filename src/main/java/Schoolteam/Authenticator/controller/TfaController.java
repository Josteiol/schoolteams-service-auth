package Schoolteam.Authenticator.controller;

import Schoolteam.Authenticator.logic.JwtKey;
import Schoolteam.Authenticator.models.LogedInnUser;
import Schoolteam.Authenticator.models.TokenReturn;
import Schoolteam.Authenticator.models.UserCode;
import Schoolteam.Authenticator.models.Username;
import Schoolteam.Authenticator.twoFactorAuth.TfaGoogleAuth;
import io.jsonwebtoken.Claims;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/2fa")
@CrossOrigin("*")
public class TfaController {


    @PostMapping()
    public ResponseEntity<TokenReturn> verifyCode(@RequestBody UserCode userCode){

        try{
            Claims claim = JwtKey.decodeJWT(userCode.getToken().replace("Bearer ", ""));
            if(!claim.getIssuer().equals(System.getenv("JWT_2FA_ISSUER"))){
                return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
            }

            TfaGoogleAuth googleAuth = new TfaGoogleAuth();
            if(googleAuth.verifyCode(claim.getId(), userCode.getCode())){
                String returnToken = "Bearer " + JwtKey.createJWT(claim.getId(), System.getenv("JWT_ISSUER"), claim.getSubject(), -1);
                // Build return object
                TokenReturn tokenReturn = new TokenReturn(returnToken);
                return new ResponseEntity<>(tokenReturn, HttpStatus.OK);
            }

        } catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        }



        return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
    }

    @PostMapping("/generate")
    public HttpStatus generateCode(@RequestBody Username username){
        TfaGoogleAuth googleAuth = new TfaGoogleAuth();
        String url = googleAuth.generateGoogleKey(username.getUsername());
        if(url == null){
            return HttpStatus.BAD_REQUEST;
        }
        return HttpStatus.OK;
    }

}
