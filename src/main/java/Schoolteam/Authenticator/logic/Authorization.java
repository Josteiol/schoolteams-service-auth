package Schoolteam.Authenticator.logic;

public class Authorization {

    public static boolean authorize(String service, String userAccess) {


        if (userAccess.equals("1")) {
            return true;
        }
        if (service.equals("coach") && userAccess.equals("2")) {
            return true;
        }
        if (service.equals("parent") && userAccess.equals("3")) {
            return true;
        }
        if (service.equals("player") && userAccess.equals("4")) {
            return true;
        } else {
            return false;
        }
    }
}
