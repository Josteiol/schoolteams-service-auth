package Schoolteam.Authenticator.twoFactorAuth;

import Schoolteam.Authenticator.repository.TfaRepository;
import com.warrenstrange.googleauth.GoogleAuthenticator;
import org.springframework.http.HttpStatus;


public class TfaGoogleAuth {
    private GoogleAuthenticator gAuth = new GoogleAuthenticator();
    private TfaRepository tfaRepository = new TfaRepository();

    /**
     * Generates a google authenticator secret and url
     * @param username The users username
     * @return The url for the qr code
     */
    public String generateGoogleKey(String username){
        String userEmail = tfaRepository.getUserEmail(username);
        if(userEmail == null){
            return null;
        }

        final String key = gAuth.createCredentials().getKey();
        String issuer = "School Teams %28" + userEmail + "%29";


        HttpStatus status = tfaRepository.addKeyToDatabase(username, key);
        if(status != HttpStatus.OK){
            return null;
        }

        String url = getGAuthBarCode(issuer, key);
        status = tfaRepository.addUrlToDatabase(username, url);
        if(status != HttpStatus.OK){
            return null;
        }

        return url;
    }

    /**
     * Verify code from user
     * @param codeFromUser The code from user
     * @return If the code is correct or not
     */
    public boolean verifyCode(String username, int codeFromUser){
        String key = tfaRepository.getKey(username);
        if(key == null){
            return false;
        }
        return gAuth.authorize(key, codeFromUser);
    }


    /**
     * Generates a url for the google authentication qr code
     * @param appName The name of the app
     * @param secret The users secret
     * @return The url for the qr code
     */
    private String getGAuthBarCode(String appName, String secret) {
        String url = "https://api.qrserver.com/v1/create-qr-code/?data=otpauth://totp/"+appName+"?secret="+secret+"&issuer=School%20Teams%20Team &size=200x200&ecc=M";
        return url.replace(" ","%20");
    }
}
