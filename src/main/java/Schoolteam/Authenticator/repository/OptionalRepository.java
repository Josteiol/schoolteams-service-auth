package Schoolteam.Authenticator.repository;

import Schoolteam.Authenticator.models.Optional;

import Schoolteam.Authenticator.models.Pair;
import org.springframework.http.HttpStatus;

import java.sql.*;


public class OptionalRepository {

    //Setting up the connection object
    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection conn = null;

    /**
     * Post a new optional
     * @param optional The optional to post
     * @return The http status of the request
     */
    public Pair<HttpStatus, Integer> addOptional(Optional optional){

        HttpStatus success;
        int last_inserted_id;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("INSERT INTO public.optional(date_of_birth, mobile_number, profile_picture, medical_notes)\n" +
                            " VALUES (?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
            prep.setObject(1, optional.getDate_of_birth());
            prep.setString(2, optional.getMobile_number());
            prep.setString(3, optional.getProfile_picture());
            prep.setString(4, optional.getMedical_notes());


            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.CREATED;
                ResultSet rs = prep.getGeneratedKeys();
                if(rs.next())
                {
                    last_inserted_id = rs.getInt(1);
                    System.out.println(last_inserted_id);
                    return new Pair<>(success, last_inserted_id);
                }
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return new Pair<>(HttpStatus.INTERNAL_SERVER_ERROR, -1);
        } finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return new Pair<>(success, -1);
    }
}
