package Schoolteam.Authenticator.repository;

import org.springframework.http.HttpStatus;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class TfaRepository {

    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection conn = null;

    /**
     * Gets users email from database
     * @param username The users username
     * @return The email address
     */
    public String getUserEmail(String username){

        String email = null;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT email FROM public.\"users\"" +
                                                                " INNER JOIN person" +
                                                                " ON person.id = users.person_id" +
                                                                " WHERE username = ?");
            prep.setString(1, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                email = result.getString("email");
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return email;
    }

    /**
     * Add users google auth key to database
     * @param username The users username
     * @param key The users key
     * @return The response status
     */
    public HttpStatus addKeyToDatabase(String username, String key){
        HttpStatus success;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("UPDATE public.users SET tfa_key=?" +
                            " WHERE username = ?;");
            prep.setString(1, key);
            prep.setString(2, username);

            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.OK;
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        } finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return success;
    }

    /**
     * Adds the url for the qr code in the database
     * @param username The users username
     * @param url The url
     * @return The response status
     */
    public HttpStatus addUrlToDatabase(String username, String url){
        HttpStatus success;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("UPDATE public.users SET tfa_qr_url=?" +
                            " WHERE username = ?;");
            prep.setString(1, url);
            prep.setString(2, username);

            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.OK;
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        } finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return success;
    }

    /**
     * Gets users key
     * @param username The users username
     * @return The key
     */
    public String getKey(String username){

        String key = null;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT tfa_key FROM public.\"users\" WHERE username = ?");
            prep.setString(1, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                key = result.getString("tfa_key");
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return key;
    }

}
