package Schoolteam.Authenticator.repository;

import Schoolteam.Authenticator.models.User;
import Schoolteam.Authenticator.models.ChildParent;
import org.springframework.http.HttpStatus;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class UserRepository {

    //Setting up the connection object
    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection conn = null;

    /**
     * Get a specific user
     * @param username The users username
     * @return The user
     */
    public User getSpecificUser(String username){
        User user = null;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM public.\"users\" WHERE username = ?");
            prep.setString(1, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                user = new User(
                        result.getString("username"),
                        result.getString("password"),
                        result.getInt("person_id"),
                        result.getInt("role_id"),
                        result.getBoolean("2fa_active")
                );
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return user;
    }

    /**
     * Post a new user
     * @param user The user to post
     * @return The http status of the request
     */
    public HttpStatus addUser(User user){

        HttpStatus success;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("INSERT INTO public.users(username, password, person_id, role_id)" +
                            " VALUES (?, ?, ?, ?);");
            prep.setString(1, user.getUsername());
            prep.setString(2, user.getPassword());
            prep.setInt(3, user.getPerson_id());
            prep.setInt(4, user.getRole_id());


            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.CREATED;
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        } finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return success;
    }

    /**
     * Post a new child parent relationship
     * @param childParent The child parent relationship
     * @return The response status
     */
    public HttpStatus addChildParent(ChildParent childParent){

        HttpStatus success;

        try {
            boolean childExists = false;
            conn = DriverManager.getConnection(URL);
            PreparedStatement prepSelect =
                    conn.prepareStatement("SELECT * FROM public.child_parent WHERE child = ?");
            prepSelect.setString(1, childParent.getChildUsername());

            ResultSet resultSet = prepSelect.executeQuery();

            while(resultSet.next()){
                childExists = true;
            }

            PreparedStatement prep;
            if(childExists){
                prep =
                        conn.prepareStatement("UPDATE public.child_parent SET parent_1=?, parent_2=? WHERE child = ?;");
                prep.setString(1, childParent.getParent1Username());
                prep.setString(2, childParent.getParent2Username());
                prep.setString(3, childParent.getChildUsername());
            }else{
                prep =
                        conn.prepareStatement("INSERT INTO public.child_parent(parent_1, child, parent_2)" +
                                " VALUES (?, ?, ?);");
                prep.setString(1, childParent.getParent1Username());
                prep.setString(2, childParent.getChildUsername());
                prep.setString(3, childParent.getParent2Username());
            }

            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.CREATED;
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        } finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return success;
    }
}
