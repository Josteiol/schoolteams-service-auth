package Schoolteam.Authenticator.repository;

import Schoolteam.Authenticator.models.Pair;
import Schoolteam.Authenticator.models.Person;

import org.springframework.http.HttpStatus;

import java.sql.*;

public class PersonRepository {

    //Setting up the connection object
    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection conn = null;

    /**
     * Post a new person
     * @param person The person to post
     * @return The http status of the request
     */
    public Pair<HttpStatus, Integer> addPerson(Person person){

        HttpStatus success;
        int last_inserted_id;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("INSERT INTO public.person(firstname, lastname, email, gender, optional_id)" +
                            " VALUES (?, ?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
            prep.setString(1, person.getFirstname());
            prep.setString(2, person.getLastname());
            prep.setString(3, person.getEmail());
            prep.setString(4, person.getGender());
            prep.setInt(5, person.getOptional_id());


            int result = prep.executeUpdate();

            if(result != 0){
                success = HttpStatus.CREATED;
                ResultSet rs = prep.getGeneratedKeys();
                if(rs.next())
                {
                    last_inserted_id = rs.getInt(1);
                    return new Pair<>(success, last_inserted_id);
                }
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return new Pair<>(HttpStatus.INTERNAL_SERVER_ERROR, -1);
        } finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return new Pair<>(success, -1);
    }
}
